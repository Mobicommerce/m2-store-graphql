type StoreConfig {
    mc_home_id : Int! @doc(description: "Mobicommerce CMS Home Page")
    mc_is_rtl: Boolean @doc(description: "is RTL supported ?")

    mc_headless : Boolean @doc(description: "Mobicommerce PWA")
    mc_headless_url : String! @doc(description: "Mobicommerce PWA Url")

    mc_cat_ids : String @doc(description: "Mobicommerce Home Categories Ids")

    mc_cms_pids : [McCmsPages] @doc(description: "Mobicommerce CMS Page Ids") @resolver(class: "Mobicommerce\\StoreGraphQl\\Model\\Resolver\\StoreConfigDynamicRowsResolver")
    mc_social_pages : [McSocialPages] @doc(description: "Mobicommerce Social Pages") @resolver(class: "Mobicommerce\\StoreGraphQl\\Model\\Resolver\\StoreConfigDynamicRowsResolver")
    mc_an_img : String @doc(description: "Mobicommerce Android App Image")
    mc_an_url : String @doc(description: "Mobicommerce Android App Url")   
    mc_ios_img : String @doc(description: "Mobicommerce Ios App Image")
    mc_ios_url : String @doc(description: "Mobicommerce Ios App Url") 

    mc_phone : String @doc(description: "Mobicommerce Footer Phone number") 
    mc_email : String @doc(description: "Mobicommerce Footer Email") 
    mc_address : String @doc(description: "Mobicommerce Footer Address") 
    mc_copyright : String @doc(description: "Mobicommerce Footer Copyright")
    mc_rating : Boolean @doc(description: "Mobicommerce Features - Rating Status")
    mc_wishlist : Boolean @doc(description: "Mobicommerce Features - Whishlist Status")
    mc_sharing : Boolean @doc(description: "Mobicommerce Features - Sharing Status")
    mc_coupons : Boolean @doc(description: "Mobicommerce Features - Coupons Status")
    mc_search : Boolean @doc(description: "Mobicommerce Features - Search Status")
    mc_guest_checkout : Boolean @doc(description: "Mobicommerce Features - Guest Checkout")
    mc_all_cat : Boolean @doc(description: "Mobicommerce Features - Enable All Categories")
    mc_social_login : Boolean @doc(description: "Mobicommerce Features - Social Login")
    mc_auto_filter : Boolean @doc(description: "Mobicommerce Features - Auto Filter Status")

    mc_pl_name : Boolean @doc(description: "Mobicommerce Product List - Show Name")
    mc_pl_price : Boolean @doc(description: "Mobicommerce Product List - Show Price")
    mc_pl_rating : Boolean @doc(description: "Mobicommerce Product List - Show Rating")
    mc_pl_sorting : Boolean @doc(description: "Mobicommerce Product List - Sorting Status")

    mc_pl_dsorting : String @doc(description: "Mobicommerce Product List - Default Sorting")
    mc_pl_dview : String @doc(description: "Mobicommerce Product List - Default View")
    mc_pl_cview : Boolean @doc(description: "Mobicommerce Product List - Change View Status")
    mc_pl_pview : Boolean @doc(description: "Mobicommerce Product List - Persist View Status")
    mc_pl_filters : Boolean @doc(description: "Mobicommerce Product List - Filters Status")

    mc_pd_rps : Boolean @doc(description: "Mobicommerce Product Detail - Show Related Products")
    mc_pd_cps : Boolean @doc(description: "Mobicommerce Product Detail - Show Cross Sell Products")
    mc_pd_ups : Boolean @doc(description: "Mobicommerce Product Detail - Show Upsell Products")


    mc_fp_enable : Boolean @doc(description: "Mobicommerce Analytics - Enable Facebook Pixel")
    mc_ga_enable : Boolean @doc(description: "Mobicommerce Analytics - Enable Google Analytics")
    mc_gtm_enable : Boolean @doc(description: "Mobicommerce Analytics - Enable Google Tag Manager")

    mc_fp_key : String @doc(description: "Mobicommerce Analytics - Facebook Key")
    mc_ga_key : String @doc(description: "Mobicommerce Analytics - Google Analytics Key" )
    mc_gtm_key : String @doc(description: "Mobicommerce Analytics - Google Tag Manager Key")

    mc_reward : Boolean @doc(description: "Mobicommerce Reward Point Solution - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\RewardPoint")
    mc_grocery : Boolean @doc(description: "Mobicommerce Reward Point Solution - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\Grocery")
    mc_location : Boolean @doc(description: "Mobicommerce Reward Point Solution - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\Location")
    mc_otp : Boolean @doc(description: "Mobicommerce Reward Point Solution - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\Otp")
    mc_mobile_registration : Boolean @doc(description: "Mobicommerce Reward Point Solution - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\MobileRegistration")
    mc_gmp : Boolean @doc(description: "Mobicommerce Generic Marketplace Solution - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\Gmp")
    mc_dd_enabled : Boolean @doc(description: "Mobicommerce Delivery Date Solution - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\DeliveryDateEnable")
    mc_dd_required : Boolean @doc(description: "Mobicommerce Delivery Date Required - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\DeliveryDateRequired")
    mc_dd_c_visible : Boolean @doc(description: "Mobicommerce Delivery Date Comment Visible - Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\DeliveryDateCVisible")
    mc_mobile_login : Boolean @doc(description: "Mobicommerce LoginWithMobile Module Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\LoginWithMobileEnable")
    mc_mw_enable : Boolean @doc(description: "Mobicommerce Multiwebsite Module Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\MultiWebsiteEnable")
    mc_mw_is_store_landing : Boolean @doc(description: "Mobicommerce Multiwebsite Landing Page Status") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\MultiWebsiteIsStoreLanding")
    mc_mw_store_cms_page : Int @doc(description: "Mobicommerce Multiwebsite CMS Landing page Id") @resolver(class: "\\Mobicommerce\\StoreGraphQl\\Model\\Resolver\\Module\\MultiWebsiteStoreLandingCms")
}

type McCmsPages @doc(description: "The McCmsPages query returns information about McCmsPages") {
    cms_page: String @doc(description: "cms_page")
    position: Int @doc(description: "position")
}

type McSocialPages @doc(description: "The McSocialPages query returns information about McSocialPages") {
    platform: String @doc(description: "platform")
    url: String @doc(description: "url")
    position: Int @doc(description: "position")

}