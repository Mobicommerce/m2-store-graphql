<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_StoreGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\StoreGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\StoreGraphQl\Model\Resolver\Store\StoreConfigDataProvider;

class StoreConfigDynamicRowsResolver implements ResolverInterface
{
    /**
     * @var StoreConfigDataProvider
     */
    private $storeConfigDataProvider;

    /**
     * @param StoreConfigDataProvider $storeConfigsDataProvider
     */
    public function __construct(
        StoreConfigDataProvider $storeConfigsDataProvider
    ) {
        $this->storeConfigDataProvider = $storeConfigsDataProvider;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $config = $field->getName();
        $values = $this->storeConfigDataProvider->getStoreConfigData($context->getExtensionAttributes()->getStore());

        $value = $values[$config];

        if ($value && !is_array($value) && $this->isJson($value)) {
            $value = json_decode($value, true);
        }
        
        $result = [];
        if (is_array($value)) {
            foreach ($value as $dynamicRowKey => $dynamicRowElement) {
                $result[] = $dynamicRowElement;
            }
        }

        return $result;
    }

    public function isJson($string)
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}
